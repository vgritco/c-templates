using Newtonsoft.Json;
using RestSharp;
using System;

namespace QuickStart
{
    class CreatePageContent
    {
        static void Main()
        {
            var myObject = new RootObject();
            myObject.userId = 1;
            myObject.id = 105;
            myObject.title = "foo";
            myObject.body = "bar";

            var json = JsonConvert.SerializeObject(myObject);

            var client = new RestClient("https://jsonplaceholder.typicode.com/");
            var request = new RestRequest("posts", Method.POST);

            request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);
            request.RequestFormat = DataFormat.Json;

            //var client = new RestClient("https://api.twilio.com/");
            //var request = new RestRequest("2010-04-01", Method.GET);

            IRestResponse response = client.Execute(request);
            var content = response.Content;

            Console.WriteLine(response.Content.ToString());
            Console.WriteLine($"{(int)response.StatusCode} - {response.StatusCode}");
        }
    }
}
