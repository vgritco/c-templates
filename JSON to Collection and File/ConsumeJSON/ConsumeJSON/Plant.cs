﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsumeJSON
{
    public class Plant
    {
        public string id { get; set; }
        public string genus { get; set; }
        public string species { get; set; }
        public string cultivar { get; set; }
        public string common { get; set; }
    }
}
