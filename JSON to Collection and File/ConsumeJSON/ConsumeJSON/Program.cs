﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace ConsumeJSON
{
    class Program
    {
        static void Main(string[] args)
        {
            var client = new RestClient("http://plantplaces.com/perl/mobile/viewplantsjson.pl?Combined_Name=Redbud");
            var request = new RestRequest(Method.GET);

            request.AddParameter("application/json; charset=utf-8", ParameterType.RequestBody);
            request.RequestFormat = DataFormat.Json;

            IRestResponse response = client.Execute(request);

            PlantCollection plantCollection = JsonConvert.DeserializeObject<PlantCollection>(response.Content);

            Console.WriteLine(response.Content.ToString());
            Console.WriteLine($"{(int)response.StatusCode} - {response.StatusCode}");
            Console.WriteLine(plantCollection.plants.Count);

            File.WriteAllText(@"c:\plants.json", JsonConvert.SerializeObject(plantCollection, Formatting.Indented));

            var json = JsonConvert.SerializeObject(plantCollection, Formatting.Indented);

           //var request1 = new RestRequest(Method.POST);
           // request1.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);
           // request1.RequestFormat = DataFormat.Json;

           // IRestResponse response1 = client.Execute(request1);

           // Console.WriteLine(response1.Content.ToString());
           // Console.WriteLine($"{(int)response1.StatusCode} - {response1.StatusCode}");
        }
    }
}
